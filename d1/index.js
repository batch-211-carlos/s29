/*
MongoDB - Query Operators
Expand Queries in MongoDB using Query Operators
*/

/*
Overview:

Query Operator
-Definition
-Importance

Types of Query Operators
1. Comparison Query Operators
	1.1 Greater Than 
	1.2 Greater than or equal to 
	1.3 Less Than
	1.4 Less Than or Equal to
	1.5 Not Equal to
	1.6 In
2. Evaluation Query Operator
	2.1 Regex
		2.1.1 Case Sensitive Query
		2.1.2 Case Insensitive Query
3. Logical Query Operators
	3.1 OR
	3.2 AND

Field Projection
1. Inclusion 
	1.1 Returning Specific fields in embedded documents
	1.2 Exception to the inclusion rule
	1.3 Slice Operator
2. Exclusion
	2.1 Excluding specific fields in embedded documents
*/

/*
Whar does Query Operator Mean?
- A "Query" is a request for data from a database
- An "Operator" is a symbol that represents an action or a process
- Putting them together, they mean the things that we can do on our queries using a certain operators
*/

/*
Why do we need to study Query Operators?
- Knowing query operators will enable us to create a queries that can do more that what simple operators do. (We can do more than what we do in simple CRUD Operations)
-For Example, in our CRUD operations discussion, we have discussed findOne using a specific value inside its single or multiple parameters. When we know query operators, we may look for records that are more specific
*/

//1. COMPARISON Query Operators
/*
	Includes 
		1.1 Greater Than 
		1.2 Greater than or equal to 
		1.3 Less Than
		1.4 Less Than or Equal to
		1.5 Not Equal to
		1.6 In
*/

/*
	1.1 Greater Than Operator: "$gt"
	- "$gt" finds documents that have field numbers that are greater than a specified value
	Syntax:
		- db.collectionName.find({field: {$gt: value}})
*/

db.users.find({age: {$gt: 76}}); //Neil Armstrong with with age: 82 - will not show the age exact 76 if there is any

/*
	1.2 Greater than or equal to Operator: "$gte"
	- "$gte" finds documents that have field number values that are greater than or equal to a specific value
	Syntax:
		- db.collectionName.find({field: {$gte: value}})
*/

db.users.find({age: {$gte: 76}}); ////Neil Armstrong with with age: 82 - Now Stephen was included with the age 76

/*
	1.3 Less Than: "$lt Operator"
	- "$lt" finds documents that have field number values less than the specified value
	Syntax:
		- db.collectionName.find({field: {$lt: value}})
*/
db.users.find({age: {$lt: 65}}); //Jane Doe with age 21

/*
	1.4 Less Than or Equal to Operator: "$lte"
	- "$lte" finds documents that have field number values that are less than or equal to a specified value
	Syntax:
	- db.collectionName.find({field: {$lte: value}})
*/
db.users.find({age: {$lte: 65}}); //Jane Doe - Now Bill was included with the age of 65

/*
	1.5 Not Equal to Operator: "$ne"
	- "$ne" finds documents that have field numbers that are not equal to a specified value
	Syntax:
	- db.collectionName.find({field: {$ne: value}})

*/
db.users.find({age: {$ne: 65}}); //it will show all the documents except for age 65

/*
	1.6 In Operator: "$in"
	- "$in" finds documents with specific match criteria on one field using different values
	 - can include multiple values
	Syntax:
	- db.collectionName.find({field: {$in: value}})
*/
db.users.find({lastName: {$in: ["Hawking", "Doe"]}});
db.users.find({courses: {$in: ["HTML", "React"]}});

//2. Evaluation Query Operators
// - Evaluation operators return data based on evaluations of either individual fields or the entire collection's documents.

/*
	2.1 RegEx Operator: "$regex"
	- "$regex" is shor for regular expression.
	- They are called regular expressions because they are based on regular languages
	- Regex is used for matching strings
	- It allows us to find documents that match a specific string pattern using a regular expression
*/

/*
	Under Regex Operator
	2.1.1 Case Sensitive Query
	Syntax:
		 - db.collectionName.find({field: {$regex: 'pattern'}})
*/
db.users.find({lastName: {$regex: 'A'}}); //Shows Neil because he is the only who has 'A' on the lastName

/*
	Under Regex Operator
	2.1.2 Case Insensitive Query
	- We can run case-insensitive queries by utilizing the "i" option
	Syntax:
		 - db.collectionName.find({field: {$regex: 'pattern', $options: '$optionValue'}})
*/
db.users.find({lastName: {$regex: 'A', $options: '$i'}}); //it inclused the lowercase "A" which "a"

//MiniActivity
/*
-Find users with the letter 'e' in their firstName (capital and small letters included)
- Make sure to use a case insensitive query
- Screenshot the result from the Robo3t and sent to our batch hangouts
*/
db.users.find({firstName: {$regex: 'E', $options: '$i'}});

/*
	3.1 OR Operator: "$or"
	- "$or" finds documents that match a single criteria from multiple provided search criteria
	Syntax:
		- db.collectionName.find({$or: [{fieldA: valueA}, {filedB: valueB}]});
*/
db.users.find({$or: [{firstName: "Neil"}, {age: "25"}]}); //return 1 document only because of name and age
db.users.find({$or: [{firstName: "Neil"}, {age: {$gt:30}}]}); //return 3 document because of age even if the first value did not meet

/*
	3.1 AND Operator: "$and"
	- "$and" finds documents that match multiple criteria in a single field
	Syntax:
		- db.collectionName.find({$and: [{fieldA: valueA}, {filedB: valueB}]});
*/
db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 76}}]});

//Mini Activity
/*
-Find users with the letter 'e' in their firstName and has an age of less than or equal to 30
*/
db.users.find({$and: [{firstName: {$regex: 'E', $options: 'i'}}, {age: {$lte: 30}}]});

//Field Projection
/*
	- By default MongoDB returns the whole document, especially, when dealing with complex document, 
	- But sometimes, it is not helpful to view the whole document, expecially when dealing with complex documents
	- To help with readability, of the values returned (or sometimes, because of security reasons), we include or exclude some fields
	- in other words, we project our selected fields
*/

/*
	1. INCLUSION
		- allows us to include/add specific fields only when retrieving documents
		- The value denoted is '1' to indicate that the field is being included
		- We cannot do exclusion on field that uses inclusion projection
		Syntax:
		db.collectionName.fin({criteria}, {field:1})
*/
db.users.find({firstName: "Jane"});
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone": 1
	}

);

/*
	Subtopics of Inclusion:
	1.1 Returning Specific Fields in Embedded documents
	- The double quotations are important
*/
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone": 1
	}

);

/*
	Subtopics of Inclusion:
	1.2 Exception to the Inclusion Rule: Supressing the ID field 
	- Allow us to exclude the "_id" field when retrieving documents
	- When using field projection, field inclusion and exclusion may not be used at the same time
	- Excluding the "_id" field is ONLY exception to this rule
	Syntax:
	- db.collectionName.find({criteria}, {_id:0})
*/
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0
	}

);

/*
	Subtopics of Inclusion:
	1.3 Slice Operator: "$slice"
	- "$slice" operator allows us to retrieve only 1 element that matches the search criteria
*/
//To demonstrate, let us frist insert and view an array
db.users.insert({
	namearr: [
		{
			namea: "Juan"
		},
		{
			nameb: "Tamad"
		}
	]
});
db.users.find({
	namearr:
	{
		namea: "Juan"
	}
});

//Now, let us use the slice operator
db.users.find(
	{"namearr":
		{
			namea: "Juan"
		}
	},
		{namearr:
				{$slice: 1}
		}
	);

//Mini Activity
/*
Find users with letter 's' on their firstNames (Use the 'i' option in the $regex operator)
-Show only firstName and lastName fields and hide the _id field
*/
db.users.find(
	{
		firstName: {$regex: 's', $options: '$i'
	}}, 
	{
		firstName:1, 
		lastName: 1, 
		_id:0
	}
);